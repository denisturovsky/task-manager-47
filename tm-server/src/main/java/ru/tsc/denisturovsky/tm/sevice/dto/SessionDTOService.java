package ru.tsc.denisturovsky.tm.sevice.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.repository.dto.ISessionDTORepository;
import ru.tsc.denisturovsky.tm.api.service.IConnectionService;
import ru.tsc.denisturovsky.tm.api.service.dto.ISessionDTOService;
import ru.tsc.denisturovsky.tm.dto.model.SessionDTO;
import ru.tsc.denisturovsky.tm.repository.dto.SessionDTORepository;

import javax.persistence.EntityManager;

public final class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO, ISessionDTORepository> implements ISessionDTOService {

    public SessionDTOService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected ISessionDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionDTORepository(entityManager);
    }

}