package ru.tsc.denisturovsky.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.service.IConnectionService;
import ru.tsc.denisturovsky.tm.api.service.IPropertyService;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectTaskDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.ITaskDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.IUserDTOService;
import ru.tsc.denisturovsky.tm.dto.model.TaskDTO;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.denisturovsky.tm.exception.entity.TaskNotFoundException;
import ru.tsc.denisturovsky.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.denisturovsky.tm.exception.field.TaskIdEmptyException;
import ru.tsc.denisturovsky.tm.exception.field.UserIdEmptyException;
import ru.tsc.denisturovsky.tm.marker.UnitCategory;
import ru.tsc.denisturovsky.tm.sevice.ConnectionService;
import ru.tsc.denisturovsky.tm.sevice.PropertyService;
import ru.tsc.denisturovsky.tm.sevice.dto.ProjectDTOService;
import ru.tsc.denisturovsky.tm.sevice.dto.ProjectTaskDTOService;
import ru.tsc.denisturovsky.tm.sevice.dto.TaskDTOService;
import ru.tsc.denisturovsky.tm.sevice.dto.UserDTOService;

import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.*;
import static ru.tsc.denisturovsky.tm.constant.TaskTestData.*;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @NotNull
    private static final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private static final IUserDTOService userService = new UserDTOService(propertyService, connectionService, projectService, taskService);

    @NotNull
    private static String userId = "";

    @NotNull
    private final IProjectTaskDTOService service = new ProjectTaskDTOService(projectService, taskService);

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final UserDTO user = userService.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = userService.findByLogin(USER_TEST_LOGIN);
        if (user != null) userService.remove(user);
    }

    @After
    public void after() throws Exception {
        taskService.clear(userId);
        projectService.clear(userId);
    }

    @Before
    public void before() throws Exception {
        projectService.add(userId, USER_PROJECT1);
        projectService.add(userId, USER_PROJECT2);
        taskService.add(userId, USER_TASK1);
        taskService.add(userId, USER_TASK2);
    }

    @Test
    public void bindTaskToProject() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.bindTaskToProject(null, USER_PROJECT1.getId(), USER_TASK1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.bindTaskToProject("", USER_PROJECT1.getId(), USER_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.bindTaskToProject(userId, null, USER_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.bindTaskToProject(userId, "", USER_TASK1.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.bindTaskToProject(userId, USER_PROJECT1.getId(), null));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.bindTaskToProject(userId, USER_PROJECT1.getId(), ""));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.bindTaskToProject(userId, NON_EXISTING_PROJECT_ID, USER_TASK1.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.bindTaskToProject(userId, USER_PROJECT1.getId(), NON_EXISTING_TASK_ID));
        service.bindTaskToProject(userId, USER_PROJECT2.getId(), USER_TASK1.getId());
        @Nullable final TaskDTO task = taskService.findOneById(userId, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_PROJECT2.getId(), task.getProjectId());
        service.bindTaskToProject(userId, USER_PROJECT1.getId(), USER_TASK1.getId());
    }

    @Test
    public void removeProjectById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeProjectById(null, USER_PROJECT1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeProjectById("", USER_PROJECT1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.removeProjectById(userId, null));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.removeProjectById(userId, ""));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.removeProjectById(userId, NON_EXISTING_PROJECT_ID));
        service.bindTaskToProject(userId, USER_PROJECT1.getId(), USER_TASK1.getId());
        service.bindTaskToProject(userId, USER_PROJECT1.getId(), USER_TASK2.getId());
        service.removeProjectById(userId, USER_PROJECT1.getId());
        Assert.assertNull(projectService.findOneById(userId, USER_PROJECT1.getId()));
        Assert.assertNull(taskService.findOneById(userId, USER_TASK1.getId()));
        Assert.assertNull(taskService.findOneById(userId, USER_TASK2.getId()));
    }

    @Test
    public void unbindTaskFromProject() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.unbindTaskFromProject(null, USER_PROJECT1.getId(), USER_TASK1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.unbindTaskFromProject("", USER_PROJECT1.getId(), USER_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.unbindTaskFromProject(userId, null, USER_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.unbindTaskFromProject(userId, "", USER_TASK1.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.unbindTaskFromProject(userId, USER_PROJECT1.getId(), null));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.unbindTaskFromProject(userId, USER_PROJECT1.getId(), ""));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.unbindTaskFromProject(userId, NON_EXISTING_PROJECT_ID, USER_TASK1.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.unbindTaskFromProject(userId, USER_PROJECT1.getId(), NON_EXISTING_TASK_ID));
        service.unbindTaskFromProject(userId, USER_PROJECT1.getId(), USER_TASK1.getId());
        @Nullable final TaskDTO task = taskService.findOneById(userId, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertNull(task.getProjectId());
        service.bindTaskToProject(userId, USER_PROJECT1.getId(), USER_TASK1.getId());
    }

}